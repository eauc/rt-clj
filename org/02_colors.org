#+TITLE: 02 - Colors
#+SETUPFILE: ./publish.setup
#+PROPERTY: header-args :exports code :tangle no

[[./index.org][Documentation index]]

* Colors

  Colors are (red, green, blue) tuples.

  #+NAME: colors_create_test
  #+BEGIN_SRC clojure
  (testing "Colors are (red, green, blue) tuples"
    (let [col (color -0.5 0.4 1.7)]
      (is (= -0.5
             (red col)))
      (is (= 0.4
             (green col)))
      (is (= 1.7
             (blue col)))))
  #+END_SRC

  #+NAME: colors_create
  #+BEGIN_SRC clojure
  (def color t/tuple)

  (def black (color 0. 0. 0.))

  (def white (color 1. 1. 1.))

  (def red t/x)

  (def green t/y)

  (def blue t/z)
  #+END_SRC

** Operations

  Colors support addition, substraction and multiplication by a scalar.

  #+NAME: colors_add_sub_mul_test
  #+BEGIN_SRC clojure
  (testing "Adding colors"
    (is (t/eq? (color 1.6 0.7 1.0)
               (add (color 0.9 0.6 0.75)
                    (color 0.7 0.1 0.25)))))

  (testing "Substracting colors"
    (is (t/eq? (color 0.2 0.5 0.5)
               (sub (color 0.9 0.6 0.75)
                    (color 0.7 0.1 0.25)))))

  (testing "Multiplying a color by a scalar"
    (is (t/eq? (color 0.4 0.6 0.8)
               (mul (color 0.2 0.3 0.4) 2.0))))
  #+END_SRC

  #+NAME: colors_add_sub_mul
  #+BEGIN_SRC clojure
  (def add t/add)

  (def sub t/sub)

  (def mul t/mul)
  #+END_SRC

  We can also multiply 2 colors.

  #+NAME: colors_dot_test
  #+BEGIN_SRC clojure
  (testing "Multiplying 2 colors"
    (is (t/eq? (color 0.9 0.2 0.04)
               (dot (color 1.0 0.2 0.4)
                    (color 0.9 1.0 0.1)))))
  #+END_SRC

  #+NAME: colors_dot
  #+BEGIN_SRC clojure
  (def dot (partial map *))
  #+END_SRC

* Canvas

  [[file:../samples/canvas_ppm_example.png]]

  A canvas is just a rectangular grid of pixels.
  Every pixel in the canvas should be initialized to black by default.
  The canvas constructor also accept a custom initialization color.

  #+NAME: canvas_create_test
  #+BEGIN_SRC clojure
  (testing "Canvas creation"
    (let [c (canvas 10 20)]
      (is (= 10
             (width c)))
      (is (= 20
             (height c)))))
  #+END_SRC

  We choose to use simple vectors of rows to store the canvas.

  #+NAME: canvas_create
  #+BEGIN_SRC clojure
  (defn canvas
    ([w h col]
     (let [row (vec (repeat w col))]
       (vec (repeat h row))))
    ([w h]
     (canvas w h (c/color 0. 0. 0.))))

  (defn width [c]
    (count (first c)))

  (defn height [c]
    (count c))

  (defn pixels [c]
    (reduce concat c))
  #+END_SRC

  We can write & read pixels at specific positions in canvas.

  #+NAME: canvas_write_test
  #+BEGIN_SRC clojure
  (testing "Writing pixels to canvas"
    (is (= (c/color 0.1 0.2 0.3)
           (-> (canvas 10 20)
               (assoc-at 5 13 (c/color 0.1 0.2 0.3))
               (get-at 5 13)))))
  #+END_SRC

  #+NAME: canvas_read_write
  #+BEGIN_SRC clojure
  (defn assoc-at [c x y p]
    (assoc-in c [y x] p))

  (defn get-at [c x y]
    (get-in c [y x]))
  #+END_SRC

** PPM image format

   We can transform the canvas to a PPM image file format.

   The PPM file starts with a header, immediately followed by the pixels data, and ends with an empty line.

   #+NAME: canvas_ppm
   #+BEGIN_SRC clojure
   (defn ppm-rows [cv]
     (conj
       (into
         (ppm-header cv)
         (ppm-data cv))
       ""))
   #+END_SRC

   The first 3 rows of the file are the header:
   - the ="P3"= magic number.
   - the =width= and =height= separated by a space.
   - the maximum color value.

   #+NAME: canvas_ppm_header_test
   #+BEGIN_SRC clojure
   (testing "Constructing the PPM header"
     (is (= ["P3"
             "5 3"
             "255"]
            (take 3 (ppm-rows (canvas 5 3))))))
   #+END_SRC

   #+NAME: canvas_ppm_header
   #+BEGIN_SRC clojure
   (defn ppm-header [cv]
     ["P3"
      (str (width cv) " " (height cv))
      "255"])
   #+END_SRC

   Following the header is the pixels data.
   - each pixel is represented by 3 integers: red, green, blue.
   - each value should be scaled from =0= to =255=.
   - each element should be separated from its neighbors by a space.
   - the file should end with an empty line.

   #+NAME: canvas_ppm_data_test
   #+BEGIN_SRC clojure
   (testing "Constructing the PPM pixel data"
     (let [c1 (c/color 1.5 0. 0.)
           c2 (c/color 0. 0.5 0.)
           c3 (c/color -0.5 0. 1.)
           c (-> (canvas 5 3)
                 (assoc-at 0 0 c1)
                 (assoc-at 2 1 c2)
                 (assoc-at 4 2 c3))]
       (is (= ["255 0 0 0 0 0 0 0 0 0 0 0 0 0 0"
               "0 0 0 0 0 0 0 128 0 0 0 0 0 0 0"
               "0 0 0 0 0 0 0 0 0 0 0 0 0 0 255"
               ""]
              (subvec (ppm-rows c) 3)))))
   #+END_SRC

   No line in a PPM file should be more than 70 characters long.

   #+NAME: canvas_ppm_length_test
   #+BEGIN_SRC clojure
   (testing "Splitting long lines in PPM files"
     (let [cv (canvas 10 2 (c/color 1. 0.8 0.6))]
       (is (= ["255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204"
               "153 255 204 153 255 204 153 255 204 153 255 204 153"
               "255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204"
               "153 255 204 153 255 204 153 255 204 153 255 204 153"
               ""]
              (subvec (ppm-rows cv) 3)))))
   #+END_SRC

   #+NAME: canvas_ppm_data
   #+BEGIN_SRC clojure
   (defn ppm-clamp [v]
     (min 1. (max 0. v)))

   (defn ppm-color [col]
     (st/join " " (map #(Math/round (+ 0.49 (* 255 (ppm-clamp %)))) col)))

   (def ppm-max-line-length 70)

   (defn ppm-data-row [row]
     (let [raw (st/join " " (map ppm-color row))]
       (loop [remaining raw
              rows []]
         (if (> ppm-max-line-length (count remaining))
           (conj rows remaining)
           (let [split-index (st/last-index-of remaining " " ppm-max-line-length)]
             (recur (subs remaining (inc split-index))
                    (conj rows (subs remaining 0 split-index))))))))

   (defn ppm-data [cv]
     (flatten (map ppm-data-row cv)))
   #+END_SRC

* Files                                                            :noexport:
  :PROPERTIES:
  :header-args: :exports none :noweb yes
  :END:

   #+BEGIN_SRC clojure :tangle ../src/rt_clj/colors.clj
   (ns rt-clj.colors
     (:require [rt-clj.tuples :as t]))


   <<colors_create>>


   <<colors_add_sub_mul>>

   <<colors_dot>>
   #+END_SRC

  #+BEGIN_SRC clojure :tangle ../test/rt_clj/colors_test.clj
  (ns rt-clj.colors-test
    (:require [clojure.test :refer :all]
              [rt-clj.colors :refer :all]
              [rt-clj.tuples :as t]))

  (deftest colors-test
    <<colors_create_test>>

    <<colors_add_sub_mul_test>>

    <<colors_dot_test>>)
  #+END_SRC

   #+BEGIN_SRC clojure :tangle ../src/rt_clj/canvas.clj
   (ns rt-clj.canvas
     (:import java.lang.Math)
     (:require [clojure.string :as st]
               [rt-clj.colors :as c]))


   <<canvas_create>>

   <<canvas_read_write>>


   <<canvas_ppm_header>>

   <<canvas_ppm_data>>

   <<canvas_ppm>>
   #+END_SRC

  #+BEGIN_SRC clojure :tangle ../test/rt_clj/canvas_test.clj
  (ns rt-clj.canvas-test
    (:require [clojure.test :refer :all]
              [rt-clj.canvas :refer :all]
              [rt-clj.colors :as c]))

  (deftest canvas-test
    <<canvas_create_test>>

    <<canvas_write_test>>

    <<canvas_ppm_header_test>>

    <<canvas_ppm_data_test>>

    <<canvas_ppm_length_test>>)
  #+END_SRC
