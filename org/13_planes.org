#+TITLE: 13 - Planes
#+SETUPFILE: ./publish.setup
#+PROPERTY: header-args :exports code :tangle no

[[./index.org][Documentation index]]

[[file:../samples/planes_spheres_example.png]]

* Creation

  Planes are records implementing Shape protocol.

  #+NAME: planes_create
  #+BEGIN_SRC clojure
  (def plane
    (partial sh/shape local-bounds local-intersect local-normal))
  #+END_SRC

  Planes has a transform property.

  #+NAME: planes_trans_test
  #+BEGIN_SRC clojure
  (testing "A plane's default transformation"
    (is (= (m/id 4)
           (:transform (plane)))))

  (testing "Changing a plane's transformation"
    (let [t (tr/translation 2. 3. 4.)
          s (plane t)]
      (:transform s)))
  #+END_SRC

  Planes has a material property.

  #+NAME: planes_material_test
  #+BEGIN_SRC clojure
  (testing "A plane has a default material"
    (is (= mr/default-material
           (:material (plane)))))
  #+END_SRC

* Bounds

  #+NAME: planes_bounds
  #+BEGIN_SRC clojure
  (def local-bounds
    (constantly {:min (t/point (- t/infinity) (- t/epsilon) (- t/infinity))
                 :max (t/point t/infinity t/epsilon t/infinity)}))
  #+END_SRC


* Intersections

  The normalized plane is =y\=0=, with normal vector =n\=[0,1,0]=.

  There are 4 cases to consider:
  - the ray is parallel to the plane: no hit.
  - the ray is coplanar to the plane: no hit (planes are infinitely thins).
  - the ray origin is above the plane.
  - the ray origin is below the plane.

  #+NAME: planes_ray_int_test
  #+BEGIN_SRC clojure
  (testing "Intersect with a ray parallel to the plane"
    (is (= []
           (local-intersect
             (plane)
             (r/ray (t/point 0. 10. 0.) (t/vector 0. 0. 1.))))))

  (testing "Intersect with a coplanar plane"
    (is (= []
           (local-intersect
             (plane)
             (r/ray (t/point 0. 0. 0.) (t/vector 0. 0. 1.))))))

  (testing "A ray intersecting a plane from above"
    (let [xs (local-intersect
               (plane)
               (r/ray (t/point 0. 1. 0.) (t/vector 0. -1. 0.)))]
      (is (= 1
             (count xs)))
      (is (= 1.
             (:t (first xs))))
      (is (= (plane)
             (:object (first xs))))))

  (testing "A ray intersecting a plane from below"
    (let [xs (local-intersect
               (plane)
               (r/ray (t/point 0. -1. 0.) (t/vector 0. 1. 0.)))]
      (is (= 1
             (count xs)))
      (is (= 1.
             (:t (first xs))))
      (is (= (plane)
             (:object (first xs))))))
  #+END_SRC

  #+NAME: planes_ray_int
  #+BEGIN_SRC clojure
  (defn local-intersect [p {:keys [origin direction]}]
    (if (t/close? 0. (t/y direction))
      []
      (let [t (- (/ (t/y origin) (t/y direction)))]
        [(i/intersection t p)])))
  #+END_SRC

* Normal

  The local-normal of plane is always =[0 1 0]=.

  #+NAME: planes_norm_test
  #+BEGIN_SRC clojure
  (testing "The normal of a plane is constant everywhere"
    (is (= (t/vector 0. 1. 0.)
           (local-normal (plane) (t/point 0. 0. 0.) {})))
    (is (= (t/vector 0. 1. 0.)
           (local-normal (plane) (t/point 10. 0. -10.) {})))
    (is (= (t/vector 0. 1. 0.)
           (local-normal (plane) (t/point -5. 0. 150.) {}))))
  #+END_SRC

  #+NAME: planes_norm
  #+BEGIN_SRC clojure
  (defn local-normal [_ _ _]
    (t/vector 0. 1. 0.))
  #+END_SRC

* Files                                                            :noexport:
  :PROPERTIES:
  :header-args: :exports none :noweb yes
  :END:

  #+BEGIN_SRC clojure :tangle ../src/rt_clj/planes.clj
  (ns rt-clj.planes
    (:require [rt-clj.intersections :as i]
              [rt-clj.materials :as mr]
              [rt-clj.matrices :as m]
              [rt-clj.rays :as r]
              [rt-clj.shapes :as sh]
              [rt-clj.tuples :as t]))


  <<planes_bounds>>


  <<planes_ray_int>>


  <<planes_norm>>


  <<planes_create>>
  #+END_SRC

  #+BEGIN_SRC clojure :tangle ../test/rt_clj/planes_test.clj
  (ns rt-clj.planes-test
    (:require [clojure.test :refer :all]
              [rt-clj.planes :refer :all]
              [rt-clj.intersections :as i]
              [rt-clj.materials :as mr]
              [rt-clj.matrices :as m]
              [rt-clj.rays :as r]
              [rt-clj.shapes :as sh]
              [rt-clj.transformations :as tr]
              [rt-clj.tuples :as t]))

  (deftest planes-test
    <<planes_trans_test>>

    <<planes_material_test>>

    <<planes_ray_int_test>>

    <<planes_norm_test>>)
  #+END_SRC

