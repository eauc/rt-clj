#+TITLE: 08 - Lights
#+SETUPFILE: ./publish.setup
#+PROPERTY: header-args :exports code :tangle no

[[./index.org][Documentation index]]

* Creation

  #+NAME: lights_create_test
  #+BEGIN_SRC clojure
  (testing "A point light has a position and intensity"
    (let [intensity (c/color 1. 1. 1.)
          position (t/point 0. 0. 0.)
          light (point-light position intensity)]
      (is (= position
             (:position light)))
      (is (= intensity
             (:intensity light)))))
  #+END_SRC

  #+NAME: lights_create
  #+BEGIN_SRC clojure
  (defn point-light [position intensity]
    {:position position
     :intensity intensity})
  #+END_SRC

* Files                                                           :noexport:
   :PROPERTIES:
   :header-args: :exports none :noweb yes
   :END:

  #+BEGIN_SRC clojure :tangle ../src/rt_clj/lights.clj
  (ns rt-clj.lights)


  <<lights_create>>
  #+END_SRC

  #+BEGIN_SRC clojure :tangle ../test/rt_clj/lights_test.clj
  (ns rt-clj.lights-test
    (:require [clojure.test :refer :all]
              [rt-clj.lights :refer :all]
              [rt-clj.colors :as c]
              [rt-clj.tuples :as t]))

  (deftest lights-test
    <<lights_create_test>>)
  #+END_SRC
