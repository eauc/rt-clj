#+TITLE: 18 - Groups
#+SETUPFILE: ./publish.setup
#+PROPERTY: header-args :exports code :tangle no

[[./index.org][Documentation index]]

[[file:../samples/groups_example.png]]

* Creation

  #+NAME: groups_create
  #+BEGIN_SRC clojure
  (defn group
    ([transform children]
     (with-children
       (sh/shape local-bounds local-intersect identity transform nil)
       children))
    ([transform]
     (group transform []))
    ([]
     (group (m/id 4) [])))
  #+END_SRC

* Bounds

  #+NAME: groups_bounds_test
  #+BEGIN_SRC clojure
  (testing "The boundaries of a group are the union of its children"
    (let [gr (with-children (group)
               [(s/sphere (tr/translation 1. 2. 3.))
                (s/sphere (tr/scaling 0.5 1.5 0.5))
                (s/sphere (tr/rotation-x (/ Math/PI 4.)))])
          bs ((:local-bounds gr) gr)]
      (is (t/eq? (t/point -1. -1.5 (- (Math/sqrt 2.)))
                 (:min bs)))
      (is (= (t/point 2. 3. 4.)
                 (:max bs)))))
  #+END_SRC

  #+NAME: groups_bounds
  #+BEGIN_SRC clojure
  (def local-bounds
    (constantly {:min (t/point (- t/epsilon) (- t/epsilon) (- t/epsilon))
                 :max (t/point t/epsilon t/epsilon t/epsilon)}))

  (defn children-bounds
    [cs]
    (let [bs (map sh/bounds cs)
          min-xs (map (comp t/x :min) bs)
          min-ys (map (comp t/y :min) bs)
          min-zs (map (comp t/z :min) bs)
          max-xs (map (comp t/x :max) bs)
          max-ys (map (comp t/y :max) bs)
          max-zs (map (comp t/z :max) bs)]
      {:min (t/point (apply min min-xs)
                     (apply min min-ys)
                     (apply min min-zs))
       :max (t/point (apply max max-xs)
                     (apply max max-ys)
                     (apply max max-zs))}))
  #+END_SRC

* Children

  Children contains a reference to their parent group.

  #+NAME: groups_children_test
  #+BEGIN_SRC clojure
  (testing "Adding children to a group"
    (let [g (with-children
              (group (tr/translation 1. 2. 3.))
              [(s/sphere)])]
      (is (= (tr/translation 1. 2. 3.)
             (:transform (:parent (first (:children g))))))))
  #+END_SRC

  #+NAME: groups_children
  #+BEGIN_SRC clojure
  (defn with-parent
    [sh {:keys [material] :as p}]
    (let [new-sh (cond-> sh
                   :always (assoc :parent p)
                   (some? material) (assoc :material material))]
      (assoc new-sh :children (mapv #(with-parent % new-sh) (:children new-sh)))))

  (defn with-children
    [gr cs]
    (let [new-gr (cond-> gr
                   (not (empty? cs))
                   (assoc :local-bounds (constantly (children-bounds cs))))]
      (assoc new-gr :children (mapv #(with-parent % new-gr) cs))))
  #+END_SRC

* Intersections

  Intersecting a ray with a empty group should always return no intersections.

  Otherwise, it should returns the conjunction of all intersections with each child shape, sorted by increasing distance.

  It should correctly apply the group and its children transformations.

  #+NAME: groups_ints_test
  #+BEGIN_SRC clojure
  (testing "Intersecting a ray with an empty group"
    (is (= []
           (local-intersect (group) (r/ray (t/point 0. 0. 0.) (t/vector 0. 0. 1.))))))

  (testing "Intersecting a ray with a nonempty group"
    (let [s1 (s/sphere)
          s2 (s/sphere (tr/translation 0. 0. -3.))
          s3 (s/sphere (tr/translation 5. 0. 0.))
          g (with-children (group) [s1 s2 s3])
          r (r/ray (t/point 0. 0. -5.) (t/vector 0. 0. 1.))]
      (is (= [(nth (:children g) 1)
              (nth (:children g) 1)
              (nth (:children g) 0)
              (nth (:children g) 0)]
             (mapv :object (local-intersect g r))))))

  (testing "Intersecting a transformed group"
    (let [g (with-children
              (group (tr/scaling 2. 2. 2.))
              [(s/sphere (tr/translation 5. 0. 0.))])
          r (r/ray (t/point 10. 0. -10.) (t/vector 0. 0. 1.))]
      (is (= 2
             (count (sh/intersect g r))))))
  #+END_SRC

  #+NAME: groups_ints
  #+BEGIN_SRC clojure
  (defn local-intersect
    [{:keys [children] :as g} r]
    (let [bounds-miss? (empty? (cu/local-intersect g r))]
      (if bounds-miss?
        []
        (into []
              (sort-by :t
                       (reduce (fn [ints c]
                                 (concat ints (sh/intersect c r))) '() children))))))
  #+END_SRC

* Files                                                            :noexport:
  :PROPERTIES:
  :header-args: :exports none :noweb yes
  :END:

  #+BEGIN_SRC clojure :tangle ../src/rt_clj/groups.clj
  (ns rt-clj.groups
    (:import java.lang.Math)
    (:require [rt-clj.intersections :as i]
              [rt-clj.cubes :as cu]
              [rt-clj.matrices :as m]
              [rt-clj.materials :as mr]
              [rt-clj.shapes :as sh]
              [rt-clj.tuples :as t]))


  <<groups_bounds>>


  <<groups_children>>


  <<groups_ints>>


  <<groups_create>>
  #+END_SRC

  #+BEGIN_SRC clojure :tangle ../test/rt_clj/groups_test.clj
  (ns rt-clj.groups-test
    (:require [clojure.test :refer :all]
              [rt-clj.groups :refer :all]
              [rt-clj.rays :as r]
              [rt-clj.shapes :as sh]
              [rt-clj.spheres :as s]
              [rt-clj.transformations :as tr]
              [rt-clj.tuples :as t]))

  (deftest groups-test

    <<groups_children_test>>

    <<groups_ints_test>>

    <<groups_bounds_test>>)
  #+END_SRC
